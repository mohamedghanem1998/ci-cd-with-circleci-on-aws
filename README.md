# Hosting a Full-Stack Application

### Task: Deploying an existing NodeJS/Ionic application to AWS cloud.

---

### Features
1) CircleCI
2) AWS EBS to host backend
3) S3 to host frontend
4) RDS

NB: This project is part of Udacity Hosting a Full-Stack Application course
